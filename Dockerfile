#
# Dockerfile for tinc
#

FROM alpine:3.9@sha256:7746df395af22f04212cd25a92c1d6dbc5a06a0ca9579a229ef43008d4d1302a

RUN apk add --no-cache iptables tinc-pre

COPY rootfs /

VOLUME /etc/tinc

ENV NETNAME=netname \
    KEYSIZE=4096    \
    VERBOSE=2 \
    TINC_INTERFACE=tun0

ENV IP_ADDR=1.2.3.4       \
    IP_PORT=655      \
    ADDRESS=10.0.0.1      \
    NETMASK=255.0.0.0 \
    NETWORK=10.0.0.0/8   \
    TINC_NODENAME=server  \
    RUNMODE=server

RUN chmod +x /entrypoint.sh /init.sh         

EXPOSE 655/tcp 655/udp

ENTRYPOINT ["/entrypoint.sh"]