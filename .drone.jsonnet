local Cache(name, settings) = {
  name: name,
  image: 'drillster/drone-volume-cache',
  settings: settings,
  volumes: [
    { name: 'cache', path: '/cache' }
  ]
};

local Pipeline(name, trigger) = {
  kind: "pipeline",
  name: name,
  trigger: trigger,
  steps: [
    { name: 'preset',
      image: 'shynome/npm-build',
      commands: [
        'npm run set_tags'
      ],
    },
    Cache('restore-cache', { mount: [ '/drone/docker' ], restore: true }),
    { name: 'docker_publish',
      image: 'plugins/docker',
      settings: {
        repo: '${DRONE_REPO%%-docker}',
        dockerfile: 'Dockerfile',
        storage_path: '/drone/docker'
      },
      environment: {
        DOCKER_USERNAME: { from_secret: 'docker_username' },
        DOCKER_PASSWORD: { from_secret: 'docker_password' }
      }
    },
    Cache('rebuild-docker-cache', { mount: ['/drone/docker'], rebuild: true }),
    { name: 'report',
      image: 'shynome/alpine-drone-ci',
      when: { status: ['failure'] },
      environment: {
        REPORT_HOOK: { from_secret: 'report_hook' }
      },
      settings: { deploy: 'report' }
    }
  ],
  volumes: [
    {
      name: 'cache',
      host: { path: '/tmp/cache' },
    },
  ],
};

[
  Pipeline("dev",{ branch: [ 'dev' ], event: [ 'push' ] }),
  Pipeline("tag",{ event: [ 'tag' ] })
]
